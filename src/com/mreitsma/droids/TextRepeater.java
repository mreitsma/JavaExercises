package com.droids;

public class TextRepeater extends TextCommunicationModule {

    // Adding static final member is useless, this a a more elegant way to set
    // the default.
    private int numberOfRepetitions = 3;

    TextRepeater(String communicationText, int numberOfRepeats) {
        this(communicationText);
        this.numberOfRepetitions = numberOfRepeats;
    }

    TextRepeater(String communicationText) {
        super(communicationText);
    }

    public void work() {
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < this.numberOfRepetitions ; i++) {
            System.out.println(this.toString());
        }
    }


}
