package com.droids;

public class TextEncoder extends TextCommunicationModule  {

    TextEncoder(String communicationText) {
        super(communicationText);
    }

    public void work() {
        System.out.println(new StringBuilder(super.toString())
                .reverse().toString());
    }

}
