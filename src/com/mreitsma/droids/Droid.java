package com.droids;

public class Droid {

    private static int droidCounter;
    private final int serialNumber;
    private String designation;
    private Workable module;

    Droid(Workable module, String designation) {
        this.updateModule(module, designation);
        Droid.droidCounter++;
        this.serialNumber = Droid.droidCounter;
    }

    void updateModule(Workable module, String designation) {
        this.module = module;
        this.designation = designation;
    }

    void doWork() {
        this.module.work();
    }

    public String toString() {
        return "Designation: " + this.designation + ", serial: " +
                this.serialNumber;
    }

}
