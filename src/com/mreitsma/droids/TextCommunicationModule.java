package com.droids;

public abstract class TextCommunicationModule implements Workable {

    private String communicationText;

    TextCommunicationModule(String communicationText) {
        this.communicationText = communicationText;
    }

    public String toString() {
        return "Communication: [" + this.communicationText + "]";
    }
}
