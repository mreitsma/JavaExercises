package com.droids;

import java.util.ArrayList;

public class DroidFactoryLauncher {
    public static void main(String[] args) {
        System.out.println("Start");

        DroidAssembler da = new DroidAssembler();

        ArrayList<Droid> droids = new ArrayList<Droid>();

        droids.add(da.assemble(new TextEncoder(
                "Coco Stinkt!")));
        droids.add(da.assemble(new TextRepeater(
                "Mischa is cool!")));
        droids.add(da.assemble(new TextRepeater(
                "Coco is ook okay!", 10)));

        for (int i = 0; i < 10; i++ ) {
            droids.add(da.assemble(new TextEncoder(
                    "Ik ben spion nummer " + i)));
        }

        for (Droid droid: droids ) {
            droid.doWork();
        }

        System.out.println("End");
    }

}
