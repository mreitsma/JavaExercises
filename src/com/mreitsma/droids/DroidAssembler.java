package com.droids;

public class DroidAssembler {
    private static final String TEXT_ENCODER_DESIGNATION =
            "Text Encoder Droid";
    private static final String TEXT_REPEATER_DESIGNATION =
            "Text Repeater Droid";

    public Droid assemble(Workable module) {
        Droid droid = module instanceof TextEncoder ?
                new Droid(module, TEXT_ENCODER_DESIGNATION) :
                new Droid(module, TEXT_REPEATER_DESIGNATION);

        System.out.println("Created droid: [" + droid + "]");

        return droid;
    }
}
